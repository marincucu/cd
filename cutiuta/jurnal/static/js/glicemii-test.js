"use strict";
//TODO testare formatarea datii
    var glicemii_fixtures = [{model:"jurnal.glicemie", fields:{data_colectare:moment('2017-01-03'), valoare:123, tip_glicemie:"preprandiala"}},
                {model:"jurnal.glicemie", fields:{data_colectare:moment('2017-01-05'), valoare:234, tip_glicemie:"postprandiala"}}
                ];

    QUnit.test('hai sa testam tabelu', function(assert) {
        var fixture = $('#qunit-fixture');
                //TODO: glicemii fixture si testat 2-3 coloane
        var coloane=['data_colectare','valoare']

        fa_tabel(glicemii_fixtures, coloane, "#qunit-fixture");
        assert.equal($('table', fixture).length,1,"ai adaugat un tabel");
        assert.equal($('th', fixture).length, coloane.length,"ai nr de coloane corecte");
        assert.equal($('tr', fixture).length, glicemii_fixtures.length+1,"ai si nr de randuri corecte");
        //console.log($('tr', fixture)[1].children[1]);
        assert.equal($('tr', fixture)[0].children[0].innerText, coloane[0], "numele coloanei e corect");
    });


    QUnit.test('hai sa vedem graficu', function(assert) {
        var fixture = $('#qunit-fixture');
        fa_grafic(glicemii_fixtures, "#qunit-fixture");
        assert.equal($('.chart', fixture).length,1,"ai un grafic");
        assert.equal($('#axa_v', fixture).length,1,"cu o axa vericala");
        assert.equal($('#axa_o', fixture).length,1,"si una orizontala");
        assert.equal($('.bar', fixture).length, glicemii_fixtures.length, 'si ai cate glicemii atatea bare');
    });

    QUnit.test('hai sa testam calendaru', function(assert) {
        var fixture = $('#qunit-fixture')
        fa_calendar(glicemii_fixtures, "#qunit-fixture")
        assert.equal($('.calendar', fixture).length,1,"ai un calendar");
        assert.equal($('.month_title', fixture).length,2,"calendarul are 2 luni");
        assert.equal($('.day', fixture).length, 62-glicemii_fixtures.length, "decembrie si ianuarie au 62 de zile");
        assert.equal($('.active', fixture).length, glicemii_fixtures.length, "numarul de glicemii active e corect");
        //console.log($('.active', fixture)[0]);
    })
    
