function ia_glicemiile() { 
    $.getJSON('/glicemii-ajax/',{
        format:'json'})
        .done(function(data){
            //TODO si dacă nu are glicemii p0rnim tutorialu și punem data default data de azi!
            window.glicemii = data;
            if ($('input[name=first_item]:checked').val() == 'grafic') {
                fa_grafic(glicemii);
            } else if ($('input[name=first_item]:checked').val() == 'tabel'){
				fa_tabel(glicemii, ['data_colectare','valoare']);
            } else {
				fa_calendar(glicemii);
			};       


        });
};

function fa_tabel(data, columns, div="#grafic-glicemii-div"){
    data.forEach(function(fila){
        fila['fields']['data_colectare'] = moment(fila['fields']['data_colectare'], "YYYY-MM-DD").calendar(); //TODO formatare mai decenta poate

    });
    d3.select(div).selectAll("*").remove();
    var table = d3.select(div).append('table')
                .attr('class','table');
    var thead = table.append('thead');
    var tbody = table.append('tbody');
    thead.append('tr')
        .selectAll('th')
        .data(columns).enter()
        .append('th')
        .text(function (column){return column;});

    var rows = tbody.selectAll('tr')
                .data(data)
                .enter()
                .append('tr');
    var cells = rows.selectAll('td')
                .data(function (row){
                    return columns.map(function(column) {
                        return {column: column, value: row['fields'][column]};
                    });
                })
                .enter()
                .append('td')
                .text(function (d) {return d.value; })
                .style('background-color',function(d){ 
                    if (d.column=='data_colectare') {
                        return 'white'}
                    else if (d.column=='valoare'){
                        if (d.value<60 || d.value>200) {
                            return 'red';}
                        else {
                           return 'orange';
                        }
                    }
                    else { return 'blue' }

                    });
    return table;
};




    

function fa_calendar(date, div='#grafic-glicemii-div'){
    /*
     * specificatii:
     * calendarul are paginatie la 2 luni mergand din ziua de azi inapoi
     *
     */
    //preluat de aici:
    //http://bl.ocks.org/KathyZ/c2d4694c953419e0509b
    //https://bl.ocks.org/danbjoseph/13d9365450c27ed3bf5a568721296dcc
    date.forEach(function(fila) {
        fila['fields']['data_colectare'] = moment(fila['fields']['data_colectare'], "DD.MM.YYYY");
    })
    d3.select(div).selectAll("*").remove();
    var  width = 960,
        height = 750;
    



    var ultima_zi = date[0]['fields']['data_colectare'];
    
    ultima_zi_din_luna = ultima_zi.clone(); ultima_zi_din_luna.endOf('month');
    var prima_zi = ultima_zi_din_luna.clone(); 
    var prima_zi = prima_zi.subtract(2, 'months'); 
    var prima_zi = prima_zi.endOf('month');


    var day = d3.timeFormat("%w"),
        day_of_month = d3.timeFormat("%e"),
        day_of_year = d3.timeFormat("%j"),
        week = d3.timeFormat("%U"),
        month = d3.timeFormat("%m"),
        year = d3.timeFormat("%Y"),
        percent = d3.format(".1%"),
        format = d3.timeFormat("%Y-%m-%d");

    var color = d3.scaleQuantize()
                .domain([-.05, .05])
                .range(d3.range(11).map(function(d) {return "q"+d+"-11"}));
    var svg = d3.select(div).selectAll("svg")
                .data([2017])
                .enter().append("svg")
                .attr("width", width)
                .attr("height", height)
                .attr("class", "calendar")
                .append("g");

    var cellSize = 50;
    var no_months_in_a_row = 2;
    var shift_up = cellSize*3;
    var rect = svg.selectAll(".day")
                    .data(function(d){
                        return d3.timeDays(prima_zi, ultima_zi_din_luna);
                    })
                    .enter().append("rect")
                    .attr("class", "day")
                    .attr("width", cellSize)
                    .attr("height", cellSize)
                    .attr("x", function(d) {
                        var month_padding = 1.2*cellSize*7*((month(d)-1) % (no_months_in_a_row));
                        return (day(d)*cellSize+month_padding)/1;
                    })
                    .attr("y", function(d) {
                        var week_diff = week(d) - week(new Date(year(d), month(d)-1, 1));
                        var row_level = Math.ceil(month(d)/(no_months_in_a_row));
                        return ((week_diff*cellSize) + row_level*cellSize*8-cellSize-shift_up)-800;
                    })
                    .style("fill", "lightyellow")
                    .datum(format);
    var date_noi = date.filter(function(d) {
        return d['fields']['data_colectare'] >= prima_zi;
    });
    //asta doar in cazu in care am maimulte glicemii intr-o zi
    //var date_noi = d3.nest()
                    //.key(function(d) {
                        //return d['fields']['data_colectare']; })
                    //.rollup(function(v) { return d3.mean(v, function(d) {return d['fields']['valoare']})})
                    //.entries(date);
    //auleu!!!
    //console.log(date_noi);
    //console.info(d3.values(date_noi).map(function(d) {return d['fields']['data_colectare'];}));


    rect.filter(function(d) {
        var dati = d3.values(date_noi).map(function(d) {return d['fields']['data_colectare'].dayOfYear(); });
        
        return dati.indexOf(moment(d).dayOfYear()) >=0;
    })
        .style("fill", function(d){
            var dati = d3.values(date_noi).map(function(d) {return d['fields']['data_colectare'].dayOfYear(); });
            data_cu_glicemie = dati.indexOf(moment(d).dayOfYear());
            glicemie = date[data_cu_glicemie]['fields']['valoare'];
             if (glicemie<70){
                return "red";
            } else if (glicemie<130){
                return 'steelblue';
            } else if (glicemie<170) {
                return 'orange';
            } else {
                return 'red';
            }
        })
        .attr("class","active");


    var month_titles = svg.selectAll(".month-title")
                            .data( function(d){ return d3.timeMonths(prima_zi, ultima_zi) ;})
                            .enter().append("text")
                            .text(monthTitle)
                            .attr("class","month_title")
							.attr("x", function(d, i) {
							var month_padding = 1.2 * cellSize*7* ((month(d)-1) % (no_months_in_a_row));
							return month_padding+cellSize*3;
						  })
							.attr("y", function(d,i){
								return i+16;});
	
                            

    function monthTitle(t0) {
        var luna = t0.toLocaleString('ro-ro', { month: 'long'});
        return luna.charAt(0).toUpperCase() + luna.slice(1);
    };
    return;
    

};



                


    
function fa_grafic(date, div='#grafic-glicemii-div'){
    d3.select(div).selectAll("*").remove();
    var margin = { top:20, right:30, bottom: 30, left:40},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;
    var x = d3.scaleBand()
            .rangeRound([0, width], .05).padding(0.1);
    var y = d3.scaleLinear().range([height, 0]);
    var xAxis = d3.axisBottom()
                .scale(x);
    var yAxis = d3.axisLeft()
                .scale(y)
                .ticks(10);
    var svg = d3.select(div).append('svg')
                .attr('width',width+margin.left+margin.right)
                .attr('height', height+margin.top+margin.bottom)
                .append('g')
                .attr('transform',
                      'translate('+margin.left+','+margin.top+')')
                .attr('class','chart');

    x.domain(date.map(function(d){return d['fields']['data_colectare']}));
    y.domain([0,d3.max(date, function(d) {return d['fields']['valoare'] })]);


    svg.append("g")
        .attr('class', 'x axis')
        .attr('id','axa_v')
        .attr('transform', 'translate(0,'+height+')')
        .call(xAxis)
      .selectAll('text')
        .style('text-anchor', 'end')
        .attr('dx','-.8em')
        .attr('dy','-.55em')
        .attr('transform','rotate(-45)');

    svg.append('g')
        .attr('class', 'y axis')
        .attr('id','axa_o')
        .call(yAxis)
      .append('text')
        .attr('transform','rotate(-90)')
        .attr('y',6)
        .attr('dy','.71em')
        .attr('text-anchror','end')
        .text('Valoare');

    svg.selectAll('bar')
        .data(date)
        .enter().append('rect')
        .style('fill',function(d){
            glicemie = d['fields']['valoare'];
            if (glicemie<70){
                return "red";
            } else if (glicemie<130){
                return 'steelblue';
            } else if (glicemie<170) {
                return 'orange';
            } else {
                return 'red';
            }}
              )
        .attr('class','bar')
        .attr('x', function(d) { return x(d['fields']['data_colectare']) })
        .attr('width', x.bandwidth())
        .attr('y', function(d) { return y(d['fields']['valoare']) })
        .attr('height', function(d) {return height - y(d['fields']['valoare']) });

};
 
function getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie !==''){
        var cookies = document.cookie.split(';');
        for (var i=0;i<cookies.length;i++){
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length+1) === (name + '=')){
                cookieValue = decodeURIComponent(cookie.substring(name.length+1));
                break;
            }
        }
    }
    return cookieValue;
};
    






$(document).ready(function() {
    $('#mod-vizualizare input:radio').click(function(){
         if ($('input[name=first_item]:checked').val() == 'grafic') {
                fa_grafic(glicemii);
            } else if ($('input[name=first_item]:checked').val() == 'tabel'){
                fa_tabel(glicemii, ['data_colectare','valoare']);
            } else {
               fa_calendar(glicemii); 
            };       
    });


    $('#id_data_colectare').datetimepicker({
        locale: 'ro',
        showClose: true,
        format: 'MM/DD/YYYY',
        keepOpen: false
        });


    $('#adauga_glicemie').on('click', function(e){
        e.preventDefault();
        var submitButton = $(this);
        var form = $('#form_adauga_glicemie');
        var formData = form.serialize();
        $.ajax({
            url:'/glicemie/adauga/',//de schimbat
            type: 'POST',
            headers: {'X-CSRFToken':getCookie('csrftoken')},
            data: formData, 
            error: function(eroare){
                form.prepend(eroare.responseText);
                for(key in eroare.responseJSON){
                    console.log('eroare la câmpul', key, 'cu valoarea ', eroare.responseJSON.key);
                };
            },
            success: function(data){
                $('#erori').empty(); //TODO și updatat tabelu
            }

            });
    });
    
    ia_glicemiile();
    
});
