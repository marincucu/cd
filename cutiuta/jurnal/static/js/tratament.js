$(document).ready(function() {
    var medicamente = [{nume:"Siofor", dci:"metformin"}, {nume:"Metfogamma", dci:"metformin"},{ nume:"Diaprel", dci: "gliclazid"},{ nume:"Amaryl", dci:"glimepirid"},{nume:"Lantus", dci:"glargine"},{ nume:"Levemir", dci:"detemir"}]
    var engine = new Bloodhound({
        local: medicamente,
        identify: function(obj) {return obj.nume;},
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        remote:{
            url:'/medicamente-ajax/%QUERY/',
            wildcard: '%QUERY'
        }
    });


    $('#drugs').typeahead({
        hint: true,
        highlight: true,
        minLength:4},
        {name: 'medicamente',
            source:  engine,
            display: function(d){
                return (d['fields']['nume']+ ' '+ d['fields']['concentratie']);
            },
            afterSelect: function(d){
                //asta nu ßtiu de ce nu vrea sa apará
                console.log('sallut');
            }
        });

    $('#id_reminder_2').datetimepicker({//smf cumva dacă ăsta e după id_data_administrare nu merge

        format:'LT',
        keepOpen: false,
        locale: 'ro'
    });

     $('#id_data_administrare').datetimepicker({
        locale: 'ro',
        showClose: true,
        format: 'MM/DD/YYYY',
        keepOpen: false
        });

    


    });
