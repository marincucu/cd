'''formularele pentru aplicatie'''
from datetime import timedelta
from django import forms
from django.contrib.auth.models import User
from django.forms.models import inlineformset_factory
from jurnal.models import Pacient, Apartinator, Glicemie, Tratament, MedicamentPrescris
#TODO schimbat draqu formatu la dată



class InvitatieForm(forms.Form):
    contact_name = forms.CharField(required=True)
    contact_email = forms.EmailField(required=True)


class UserForm(forms.ModelForm):
    '''formularul pentru utilizator oricare ar fi el:
    pacient, medic, caregiver sau ce stiu eu
    '''
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class PacientForm(forms.ModelForm):
    class Meta:
        model = Pacient
        fields = ('telefon',)

class GlicemieForm(forms.ModelForm):
    '''formularul de glicemii'''
    tip_glicemie = forms.ChoiceField(choices=Glicemie.TIP_GLICEMIE, widget=forms.RadioSelect())
    masa_recoltare = forms.ChoiceField(choices=Glicemie.MASA_RECOLTARE, widget=forms.RadioSelect())
    class Meta:
        model = Glicemie
        help_texts = {
            'valoare':'mg/dL',
            'data_colectare':'mm/dd/yyyy',
            }

        fields = ('tip_glicemie', 'masa_recoltare', 'data_colectare', 'valoare',)





class TratamentForm(forms.ModelForm):
    ''' Plan de acțiune: să meargă ăsta cu tratamentul
    , apoi formsetul cu medicamente(apoi, js, widgetu de medicamente'''

    TIP_DURATA = (
            (1,'O lună'),
            (2, 'Două luni'),
            (3, 'Trei Luni'),
            )
    class Meta:
        model = Tratament
        fields = ('data_administrare', 'data_expirare', 'reminder',)


    data_administrare = forms.DateField(label="Data când ți-a fost prescrisă rețeta", input_formats=['%m/%d/%Y'])
    data_expirare = forms.ChoiceField(label="pentru câte luni?", choices= TIP_DURATA, widget=forms.RadioSelect())


    def clean_data_expirare(self):
        durata = timedelta(days=int(self.cleaned_data['data_expirare'])*30)
        data_administrare = self.cleaned_data['data_administrare']
        data_expirare = data_administrare + durata
        return data_expirare

    def clean_reminder(self):
        timp = self.cleaned_data['reminder']
        print('salut in pula mea')
        print(timp)
        return timp

class MedicamenteForm(forms.ModelForm):
    class Meta:
        model = MedicamentPrescris
        fields = ('medicament','dimineata','pranz','seara','reteta','atentionare',)

TratamentCuMedicamenteFormSet = inlineformset_factory(Tratament, MedicamentPrescris,form=MedicamenteForm, extra = 2)

