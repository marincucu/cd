# -*- coding: utf-8 -*-
import random
import string
import datetime
from datetime import date
from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from jurnal.views import  home_page
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from jurnal import models
from django.test import Client
from django.utils import timezone


from django.core import serializers
from django.core.management import call_command


class HaiSaFacemUnPacientTest(TestCase):
    """model testing mostly
    acest test testează fără interfață grafică  creearea unui user
    și cum acesta poate să își manageuiască glicemiile"""
    #TODO edit pacient și delete pacient cu glicemiile sale
    #TODO edit/delete glicemie
    #TODO pacient și sau glicemii incomplete care nu se pot valida :)
    

    def setUp(self):
        self.c = Client()
        self.pacienti = []
        self.glicemii_pacienti = []

    def genereaza_pacienti(self, numar=1):
        '''funcție care generează utilizatori, default doar unu'''
        for _ in range(numar):
            username = ''.join(random.choice(string.ascii_letters) for _ in range(8)) #ce string boss ?

            parola = ''.join(random.choice(string.ascii_letters) for _ in range(16))
            #TODO aici de consultat cu felul de parolă
            user = User.objects.create(username=username) #TODO email?
            user.set_password(parola)
            user.save()
            telefon = ''.join(str(random.randint(0, 9)) for _ in range(10))
            pacient = models.Pacient.objects.create(user=user, telefon=telefon)
            pacient.save()
            self.pacienti.append(pacient)


    def adauga_glicemii(self, pacient, numar=1):
        ''' adaugă glicemii pentru un pacient anume '''
        tip_glicemie = (u'preprandial', u'postprandial')
        masa_glicemie = (u'micdejun', u'prânz', u'cină')
        azi = timezone.now()
        user = User.objects.get(pk=pacient.user_id)
        for glicemie in range(numar):
            tipul_glicemiei = random.choice(tip_glicemie)
            masa_glicemie = random.choice(masa_glicemie)
            data_colectare = azi - datetime.timedelta(days=random.randint(1, 365))
            valoare = random.randint(15, 435)
            glicemie = models.Glicemie.objects.create(user_id=user, tip_glicemie=tipul_glicemiei,
                                masa_recoltare=masa_glicemie, data_colectare=data_colectare, valoare=valoare)
            glicemie.save()
            self.glicemii_pacienti.append(glicemie)

    def adauga_tratament_corect(self, pacient):
        '''test de adăugat tratament la un pacient un test care în care tratamentu treubie corect salvat'''
        azi = timezone.now()
        data_administrare = azi - datetime.timedelta(days=random.randint(1, 365))
        data_expirare = data_administrare + datetime.timedelta(days=random.randint(1,90))
        user = User.objects.get(pk=pacient.user_id)
        tratament = models.Tratament.objects.create(pacient_id = user, data_administrare = data_administrare,
                data_expirare = data_expirare, reminder = True)
        
        tratament.save()


    def test_un_utilizator_cu_un_tratament(self):
        '''ăsta va evolua într-un test și cu medicamente'''
        #TODO medicamente la test
        #TODO test cu tratamente incorect care să fail
        self.genereaza_pacienti()
        pacient = self.pacienti[0]
        self.adauga_tratament_corect(pacient)
        self.assertEqual(models.Tratament.objects.count(), 1)




    def test_mai_multi_utilizatori_cu_mai_multe_glicemii(self):
        nr_pacienti = 14
        nr_glicemii_per_pacient = 13#TODO verificat atributele unei glicemii
        self.genereaza_pacienti(nr_pacienti)
        for pacient in self.pacienti:
            self.adauga_glicemii(pacient, nr_glicemii_per_pacient)
        self.assertEqual(models.Pacient.objects.count(), nr_pacienti)
        self.assertEqual(models.Glicemie.objects.count(), nr_pacienti*nr_glicemii_per_pacient)

    def test_pacient_logare(self):
        ''' acest test se vrea un integration test:
            creaza un utilizator(pacient) care are glicemii'''
        user = User.objects.create(username='primu')
        user.set_password('parola')
        user.save()
        pacient = models.Pacient.objects.create(user=user, telefon='0768768401')
        pacient.save()
        response = self.c.post('/login/', {'username':'primu', 'password':'parola'}, follow=True)
        self.assertTrue(response.status_code == 200)
        response = self.c.post('/glicemie/adauga/', {'tip_glicemie':'preprandial',
            'masa_recoltare':'micdejun', 'data_colectare':'12/12/2012', 'valoare':120}, follow=True) 
        self.assertTrue(response.status_code == 204)
        glicemii = self.c.get('/glicemii-ajax/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        for glicemie in serializers.deserialize('json', glicemii.content):
            self.assertEqual(glicemie.object.valoare, 120)
            self.assertEqual(glicemie.object.tip_glicemie, 'preprandial')
            self.assertEqual(glicemie.object.masa_recoltare, 'micdejun')
            ziua_colectarii = glicemie.object.data_colectare
            ziua_introdusa = date(2012, 12, 12)
            self.assertEqual(ziua_introdusa, ziua_colectarii)


class HomePageTest(TestCase):
    '''acesta este un test cam auirea vedem dacă merită păstrat'''
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_ca_home_page_e_htmlu_corect(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode('utf-8'), expected_html)
