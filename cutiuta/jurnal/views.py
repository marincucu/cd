# -*- coding: utf-8 -*-
'''views pentru user și glicemie'''
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from django.views import generic, View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, FormMixin, UpdateView, FormView
#from django.core.urlresolvers import reverse_lazy
from django.urls import reverse
from django.db.models import Q
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMessage, send_mail

from django.core import serializers
from django.contrib.messages.views import SuccessMessageMixin

from django.contrib.auth.models import User
from jurnal.forms import UserForm, PacientForm, GlicemieForm, TratamentForm, TratamentCuMedicamenteFormSet, InvitatieForm
from .models import Glicemie, Tratament, MedicamentNou, Pacient

from invitations.models import Invitation


class PacientTrimiteInvitatie(LoginRequiredMixin, FormView):
    '''acest view trimite o invitateie pe email unui potențial aparținător'''
    form_class = InvitatieForm
    template_name = 'invitatie_apartinator.html'
    #success_url = reverse('glicemii')


    def form_valid(self, form):
        contact_name = form.cleaned_data['contact_name']
        contact_email = form.cleaned_data['contact_email']
        template = get_template('template_invitatie_apartinator.txt')
        context = Context({
            'contact_name': contact_name,
            'username' : self.request.user.username,
            })
        content = template.render(context)
        invite = Invitation.create(contact_email, inviter=self.request.user)
        invite.send_invitation(self.request) #tot nu apare returnu
        return super().form_valid(form)

def home_page(request):
    '''home pageu...probabil inutil aici'''
    return render(request, 'home.html')

class ListaGlicemii(LoginRequiredMixin, generic.ListView, FormMixin):
    '''acest view permite utilizatorilor logați să vadă doar propriile glicemii'''
    template_name = 'glicemii.html'
    context_object_name = 'lista_cu_ultimele_glicemii'
    model = Glicemie
    login_url = '/login/'
    form_class = GlicemieForm
    success_url = '/glicemii/'
    def get(self, request, *args, **kwargs):
        self.form = GlicemieForm(self.request.GET or None,) #WTF
        return super(ListaGlicemii, self).get(request, *args, **kwargs)
    def queryset(self):
        '''cum construiesc colecția de obiecte returnate'''
        return Glicemie.objects.filter(user_id=self.request.user).order_by('-data_colectare')
    def post(self, request): #de rezolvat cu form invalid
        ''' ce view returnez '''
        return NewGlicemieFormView.as_view()(request) #devine enervantă notația asta

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

class ListaGlicemiiAjax(object):
    '''acest view face același lucru ci ListaGlicemii doar că returneaza jsoane'''
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            raise Http404('aici umblăm doar cu ajax bă')
        return super(ListaGlicemiiAjax, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return (
            super(ListaGlicemiiAjax, self)
            .get_queryset()
            .filter(user_id=self.request.user).order_by('-data_colectare')
            ) #fffff js

    def get(self, request, *args, **kwargs):
        return HttpResponse(serializers.serialize('json', self.get_queryset()), content_type="application/json")

class ListaMedicamenteAjaxCuDestinatie(View):
    '''un view care te lasa sa cauti un medicamente dupa nume sau dci
    ăsta o sa dispară o vreme bună
    '''
    def get(self, request, medicament, *args, **kwargs):
        nume = Medicament.objects.filter(Q(nume__icontains = medicament) | Q(dci__icontains = medicament))

        return HttpResponse(serializers.serialize('json', nume, fields=('nume','concentratia'))) #nu merge concentratia cred

class ListaMedicamente(View):
    '''un view pt medicamente. Momentan are doar esentialele pentru ca e mult prea greu 
    și sincer inutil să folosesc nomenclatoru CNAS'''
    def get(self, request, medicament, *args, **kwargs):
        nume = MedicamentNou.objects.filter(Q(nume__icontains = medicament) | Q(dci__icontains = medicament))
        return HttpResponse(serializers.serialize('json', nume, fields=('nume','dci', 'concentratie')))

class ListaGlicemiiAjaxCuDestinatie(LoginRequiredMixin, ListaGlicemiiAjax, generic.ListView):
    '''acest view returnează glicemiile unui utilizator logat'''
    model = Glicemie




class AjaxableResponseMixin(object):
    def form_invalid(self, form):
        form.instance.user_id = self.request.user
        response = super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response



class GlicemieNoua(AjaxableResponseMixin, CreateView):
    model = Glicemie
    fields = ('tip_glicemie','masa_recoltare','data_colectare','valoare',)
    def form_valid(self, form):
        form.instance.user_id = self.request.user
        self.object = form.save()
        return HttpResponse(status=204)

class Tratament2(CreateView):
    success_url = '/glicemii/'
    model = Tratament
    fields = ('data_administrare','data_expirare',)
    template_name = 'tratament_form.html'
    def form_valid(self, form):
        form.instance.user_id = self.request.user
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())


class Tratament(LoginRequiredMixin, CreateView):
    model = Tratament
    success_url ='/glicemii/' #sau mă rog ne mai gândim
    login_url = '/login/' #TODO de setat global draq
    template_name = 'tratament_form.html'
    form_class=TratamentForm


    def form_valid(self, form):
        #form.instance.pacient_id = self.request.user
        context = self.get_context_data()
        tratament = context['tratament']
        with transaction.commit_on_success():
            form.instance.pacient_id = self.request.user
            self.object = form.save()
            return HttpResponse(status=204)
        #self.object = form.save()
        #return HttpResponse(status=204)

    '''asta e necesar pentru formset:'''
    def get_context_data(self, **kwargs):
        data = super(Tratament, self).get_context_data(**kwargs)
        if self.request.POST:
            data['tratament'] = TratamentCuMedicamenteFormSet(self.request.POST)
        else:
            data['tratament'] = TratamentCuMedicamenteFormSet()
        return data
    




class UpdatePacient(LoginRequiredMixin, UpdateView):
    '''acest view lasă pacientul să își modifice detaliile de contact,
    deocamdată doar ale lui, ulterior și să își adauge și pers de contact(caretakerși)'''

    model = Pacient
    fields = ['telefon',] #TODO să poate edita și emailu bre
    template_name = 'editeaza_pacient.html'
    success_url = '/glicemii/' #TODO poaaaate bag și eu named url nu urlu
    def get_object(self, queryset=None):
        #return get_object_or_404(User, pk = self.request.user)
        return self.request.user


class RegisterPacient(CreateView):
    '''acest view e html și înregistreaza un pacient nou'''
    template_name = 'inregistreaza_pacient.html'
    model = User
    form_class = UserForm
    success_url = '/glicemii/'

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class() #ăsta nu știu exact ce e cu el
        form = self.get_form(form_class)
        pacient_form = PacientForm()
        return self.render_to_response(
            self.get_context_data(form=form, pacient_form=pacient_form))

    def post(self, request, *args, **kwargs):
        self.object = None #de ce oare?
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        pacient_form = PacientForm(self.request.POST)
        if form.is_valid() and pacient_form.is_valid():
            return self.form_valid(form, pacient_form)
        else:
            return self.form_invalid(form, pacient_form)

    def form_valid(self, form, pacient_form):
        self.object = form.save()
        self.object.set_password(self.object.password)
        pacient_form.instance = self.object
        pacient_form.save()
        return HttpResponseRedirect(self.get_success_url())


    def form_invalid(self, form, pacient_form):
        return self.render_to_response(
            self.get_context_data(form=form, pacient_form=pacient_form))
