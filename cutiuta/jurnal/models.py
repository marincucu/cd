# -*- coding: utf-8 -*-
'''modelele pentru jurnalul de glicemii'''
from django.db import models
from django.contrib.auth.models import User




class Pacient(models.Model):
    '''
    Pacientul este un utilizator, mi-ar mai trebuii ziua de naștere și poate și sexul
    '''
    user = models.ForeignKey(User, related_name='utilizator', on_delete=models.PROTECT) #de verificat că nu cred că chiar protect
    telefon = models.CharField(max_length=13) #TODO telefon cu prefix internațional
    data_inregistrare = models.DateTimeField(auto_now_add=True)


class Apartinator(models.Model):
    '''
    Aparținătorul are unul sau mai mulți pacienți în grija, majoritatea unu haaaaai maxim doi.
    Începem cu unu
    ''' #TODO : un aparținător are mai mulți pacienți sau e însuși pacient
    user = models.ForeignKey(User, on_delete=models.PROTECT) #la fel cu 13
    telefon = models.CharField(max_length=13)
    pacient = models.ForeignKey(Pacient, on_delete=models.PROTECT)
    #activation_key = models.CharField(max_length=40)
    #key_expires = models.DateTimeField()


class Glicemie(models.Model):
    '''
    Modelul de inregistrare pentru jurnalul de glicemii
    '''
    TIP_GLICEMIE = (
        (u'preprandial', 'Înainte de masă'),
        (u'postprandial', 'La două ore după masă'),
        )
    MASA_RECOLTARE = (
        (u'micdejun', 'Mic dejun'),
        (u'prânz', 'Prânz'),
        (u'cina', 'Cină'),
        )
    #ăsta de ce nu e pă pacient? nu e ca și cum doctoru are glicemii
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    #asta nu o sa am nevoie de ea curând, în analiza MOSCOW e pă la C
    #unitate_masura = models.ForeignKey(Um, blank=False, null=True)
    tip_glicemie = models.CharField(max_length=12, choices=TIP_GLICEMIE, blank=True, null=True)
    masa_recoltare = models.CharField(max_length=8, choices=MASA_RECOLTARE, blank=True, null=True)
    data_colectare = models.DateField()
    valoare = models.PositiveIntegerField(default=120)
    data_introducere = models.DateTimeField(auto_now_add=True)
    #ok dacă ăsta să updatează de fiecare dată când salvez ce rost are modificare
    data_modificare = models.DateTimeField(auto_now=True, blank=True, null=True)
    #vreau și data de stocare și cea de modificare?
    def __unicode__(self):
        return self.valoare


class MedicamentNou(models.Model):
    '''să folosesc nomenclatoru e aiurea pt că 90% din medicamentele de diabet
    sunt 30 plus formatarea CNAS e de 2 lei'''
    nume = models.CharField(max_length=130)
    dci = models.CharField(max_length=130)
    concentratie = models.CharField(max_length=130)
    atc = models.CharField(max_length=60)
    #pentru pasul 2
    #informare = models.CharField() plm aici pui chestii despre informare
    #poza = models.ImageField(blank=True, null=True)

    def __unicode__(self):
        return self.nume
    def __str__(self):
        return self.nume + ' ' + self.concentratie

class Tratament(models.Model):
    ''' fiecare pacient are un tratament
    trebuie regandit nitel aici
    '''

    pacient_id = models.ForeignKey(User, on_delete=models.CASCADE)
    data_administrare = models.DateField()
    data_expirare = models.DateField()
    reminder = models.BooleanField()


class MedicamentPrescris(models.Model):
    '''medicamentul și modul de prescriere'''
    medicament = models.ForeignKey(MedicamentNou, on_delete=models.PROTECT)
    dimineata = models.BooleanField()
    pranz = models.BooleanField()
    seara = models.BooleanField()
    reteta = models.ForeignKey(Tratament, on_delete=models.PROTECT)
    atentionare = models.BooleanField()
