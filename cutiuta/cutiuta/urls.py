# -*- coding: utf-8 -*-
'''urlurile apicației'''
from django.conf.urls import include, url
from jurnal import views as Jurnal
from django.views.generic import TemplateView
#from jurnal import views

urlpatterns = [


    #url(r'^admin/', include(admin.site.urls)),
    url('^', include('django.contrib.auth.urls')), #safe
    url(r'^$', Jurnal.home_page, name='home'), #safe
    url(r'^glicemii/$', Jurnal.ListaGlicemii.as_view(), name='glicemii'),
    url(r'^glicemie/adauga/$', Jurnal.GlicemieNoua.as_view(), name='adauga-glicemie'),#safe
    url(r'^nou/$', Jurnal.RegisterPacient.as_view(), name='inregistreaza-pacient'), #TODO redenumit PLM
    url('^editeaza/$', Jurnal.UpdatePacient.as_view(), name='profil-pacient'),
    url(r'^glicemii-ajax/$', Jurnal.ListaGlicemiiAjaxCuDestinatie.as_view(), #safe
        name='glicemii-ajax'),
    url(r'^medicamente-ajax/(?P<medicament>[\w-]+)/$', Jurnal.ListaMedicamente.as_view(),
        name='medicamente-ajax'),
    url(r'^informare/$', TemplateView.as_view(template_name='informare.html'), name='informare'),
    url(r'^tratament/$', Jurnal.Tratament.as_view(), name='tratament'),

    url(r'^pacient/apartinator/adauga/$', Jurnal.PacientTrimiteInvitatie.as_view(), name='apartinator-nou'),
    url(r'^invitations/', include('invitations.urls', namespace='invitations')), #nu suportă namespace?
]
