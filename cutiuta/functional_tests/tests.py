# -*- coding: utf-8 -*-
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
#PLAN:
#TODO test că glicemia de la un pacient nu să vede la altul
#TODO verificare de glicemie: nu în viitor nu mai în trecut decât vârsta pacientului, nu peste 600


#pentru generare de username
import string, random
def genereaza_nume_utilizator():
    return ''.join(random.choice(string.lowercase) for _ in range(8))

class PacientNouTest(LiveServerTestCase):#TODO rename test și testat validare de glicemii
    #TODO: logout
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3) #nu îmi place asta
        self.browser.get(self.live_server_url) 

        self.nume_utilizator = genereaza_nume_utilizator()

    def gaseste_numele_de_utilizator_in_titlu(self, titlu):
        self.assertIn(self.nume_utilizator.capitalize(), titlu)

    def tearDown(self):
        self.browser.quit()

    def fa_utilizator(self):

        userbox = self.browser.find_element_by_id('id_username')
        userbox.send_keys(self.nume_utilizator)
        emailbox = self.browser.find_element_by_id('id_email')
        emailbox.send_keys('fara@nam.ro')
        parola = self.browser.find_element_by_id('id_password')
        parola.send_keys('parola')
        telefon = self.browser.find_element_by_id('id_telefon')
        telefon.send_keys('0768768401')
        self.browser.find_element_by_id('register').click()

    def logheaza_utilizatorul(self):
        userbox = self.browser.find_element_by_id('id_username')
        userbox.send_keys(self.nume_utilizator)
        parolabox = self.browser.find_element_by_id('id_password')
        parolabox.send_keys('parola')
        self.browser.find_element_by_id('loginbutton').click()

    def baga_o_glicemie(self): #TODO acest test depinde de luna în care ești 
        tipuri_glicemie = self.browser.find_element_by_id('id_tip_glicemie')
        for option in tipuri_glicemie.find_elements_by_tag_name('options'):
            if option.text == u'Înainte de masă':
                option.click()
                break
        feluri_glicemie = self.browser.find_element_by_id('id_masa_recoltare')
        for option in feluri_glicemie.find_elements_by_tag_name('options'):
            if option.text == u'Prânz':
                option.click()
                break
        valoare = self.browser.find_element_by_id('id_valoare')
        valoare.clear() 
        valoare.send_keys('78')
        
        #utilizatorul introduce o glicemie folosind calendarul
        self.browser.find_element_by_id('id_data_colectare').click() #TODO data colectării va fi ziua curentă
        #TODO schimbă luna
        luna = self.browser.find_element_by_class_name('bootstrap-datetimepicker-widget') #meh dacă am două dăți cu widget în același formular?
        randuri = luna.find_elements_by_tag_name('tr')
        for rand in randuri:
            coloane = rand.find_elements_by_tag_name('td')

            for element in coloane:
                if element.text == '27': #TODO aparent bagă doar ziua curentă
                    element.click()
                    return





    def test_intra_pa_site2(self):
        #TODO de documentat
        self.browser.find_element_by_link_text('Jurnalul de glicemii').click()
        self.browser.find_element_by_id('register').click()
        self.fa_utilizator()
        self.browser.find_element_by_id('loginbutton').click()
        self.logheaza_utilizatorul()
        tabel = self.browser.find_element_by_id('tabel-glicemii')
        randuri = tabel.find_elements_by_tag_name('td')
        self.assertTrue(
                any(rand.text == u'Nu ai adăugat încă nici o glicemie' for rand in randuri)
                )
        self.baga_o_glicemie()
        self.browser.find_element_by_id('adauga_glicemie').click()#WTF!

        tabel = self.browser.find_element_by_id('tabel-glicemii')
        randuri = tabel.find_elements_by_tag_name('tr')
        for rand in randuri: #TODO: de testat că vin în ordine cronologică
            coloana = rand.find_elements_by_tag_name('td')
            for numar_coloana, celula in enumerate(coloana):
                if numar_coloana==0:
                    self.assertTrue(celula.text == u'78')
                if numar_coloana==1:
                    self.assertTrue(celula.text== u'29 Octombrie 2016')
        self.browser.find_element_by_link_text('Logout').click()
        self.assertIn('Cutiuta Diabeticulu', self.browser.title)





if __name__=='__main__':
    unittest.main() #argumentu warnings = 'ignore' nu merge


