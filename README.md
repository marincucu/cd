# Cutiuța diabeticului #

Aceasta se vrea o platformă online a pacientului cu diabet. 
### Ce vrea ea să facă ? ###

O descriere în detaliu și cu exprimare mai aleasă găsiți [aici](http://www.medigate.ro/project/cutiuta-diabeticului/)

* Jurnal de glicemii
* Jurnal și reminder de pastile/insulină
* Jurnal și reminder de consulturi și analize
* Integrarea rolurilor de pacient, îngrijitor și medic
* Comunitate de pacienți
* Informare corespunzătoare dată de medici specialiști 

### Partea tehnică ###

* POSTGRES
* DJANGO
* Gunicorn
* nginx
* javascript

peste toate e TDD și git


### Cum pot să ajut ? ###

* ca programator orice commit e binevenit
* ca designer sau tester ne poți contacta

